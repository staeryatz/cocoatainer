//
//  main.m
//  CocoatainerExample
//
//  Created by Jeffrey Bakker on 2015-05-13.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "_ExampleCodeLivesHere.h"

int main(int argc, char * argv[])
{
    [_ExampleCodeLivesHere runCocoaMugExample1];
}
