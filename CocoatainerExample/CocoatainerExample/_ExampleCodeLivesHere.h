//
//  _ExampleCodeLivesHere.h
//  CocoatainerExample
//
//  Created by Jeffrey Bakker on 2015-05-17.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface _ExampleCodeLivesHere : NSObject

+(void)runCocoaMugExample1;

@end
