//
//  IDependsOn1.h
//  Cocoatainer
//
//  Created by Jeffrey Bakker on 2015-05-15.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IDependsOn1A <NSObject>
@end

@protocol IDependsOn1B <NSObject>
@end
