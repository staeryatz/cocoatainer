//
//  IDependsOn2.h
//  Cocoatainer
//
//  Created by Jeffrey Bakker on 2015-05-15.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IDependsOn2A <NSObject>
@end

@protocol IDependsOn2B <NSObject>
@end

@protocol IDependsOn2C <NSObject>
@end

@protocol IDependsOn2Startables <NSObject>
@end

@protocol IDependsOn2StartablesAB <NSObject>
@end

@protocol IDependsOn2StartablesBA <NSObject>
@end
