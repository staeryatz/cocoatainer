//
//  INoDeps.h
//  Cocoatainer
//
//  Created by Jeffrey Bakker on 2015-05-15.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol INoDepsA <NSObject>
@end

@protocol INoDepsB <NSObject>
@end

@protocol INoDepsC <NSObject>
@end