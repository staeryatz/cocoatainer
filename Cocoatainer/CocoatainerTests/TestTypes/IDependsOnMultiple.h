//
//  IDependsOnMultiple.h
//  Cocoatainer
//
//  Created by Jeffrey Bakker on 2015-05-17.
//  Copyright (c) 2015 Jeffrey Bakker. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IDependsOnMultiple <NSObject>
@property (nonatomic) NSArray *injections;
@end
